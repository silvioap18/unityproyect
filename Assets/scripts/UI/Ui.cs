﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Ui : MonoBehaviour
{
    public TextMeshProUGUI textCoins;
    public int coins;

    private void Start()
    {
        coins = 0;
        updateCounter();
    }

    public void updateCounter()
    {
        textCoins.text = coins.ToString();
    }
}