﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarBehaviourPlayer : MonoBehaviour
{
    public Slider slider;
    public Color low;
    public Color high;

    public void SetHealth(float health, float maxHealth)
    {
        slider.gameObject.SetActive(health < maxHealth);
        slider.value = health;
        slider.maxValue = maxHealth;

        slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(low, high, slider.normalizedValue);
    }

    private void Start()
    {
        slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(low, high, slider.normalizedValue);
    }


}