﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public string sLevelToLoad;

    public Animator transition;
    public float transitionTime = 4.2f;

    private void OnTriggerEnter2D(Collider2D col)
    {
        var collisionGameObject = col.gameObject;

        if (collisionGameObject.name == "Player")
        {
            LoadNextLevel();
        }
        
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sLevelToLoad);
        
    }
}