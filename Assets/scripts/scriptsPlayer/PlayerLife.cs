﻿using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    public int maxHealth;
    private int currentHealth;
    public HealthbarBehaviourPlayer healthbar;
    
    public Animator animator;
    
    private void Start()
    {
        currentHealth = maxHealth;
        healthbar.SetHealth(currentHealth, maxHealth);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        
        animator.SetTrigger("Hurt");
        print("Current health: " + currentHealth);
        healthbar.SetHealth(currentHealth, maxHealth);
        if (currentHealth <= 0)
        {
            Die();
        }
    }
    
    private void Die()
    {
        Debug.Log("Player died!");

        animator.SetBool("IsDead", true);

        GetComponent<Rigidbody2D>().gravityScale = 0;
        GetComponent<Collider2D>().isTrigger = true;
        this.enabled = false;

    }
    
}