using System;
using UnityEngine;
using Object = System.Object;

public class PlayerMovement : MonoBehaviour {
    
    [SerializeField] public CharacterController2D controller;
    [SerializeField] public Animator animator;
    [SerializeField] public GameObject player;
    [SerializeField] private Collider2D collider;
    [SerializeField] private Rigidbody2D rigibody;
    [SerializeField] private Ui ui;

    public float runSpeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch;

    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
            
        }else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

        CheckForGround();
        checkJump();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("coin"))
        {
            Destroy(col.gameObject);
            ui.coins++;
            ui.updateCounter();
        }
    }

    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }

    private void checkJump()
    {
        animator.SetFloat("jumpVelocity", rigibody.velocity.y);
    }
    private void CheckForGround()
    {
        if (collider.IsTouchingLayers(LayerMask.GetMask("FloorWalls")))
        {
            animator.SetBool("isGrounded", true);
        }
        else
        {
            animator.SetBool("isGrounded", false);
        }

    }

}