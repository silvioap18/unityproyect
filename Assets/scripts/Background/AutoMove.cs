namespace DefaultNamespace
{
    using UnityEngine;
    
    public class AutoMove : MonoBehaviour
    {
        
        public float speed = 1f;
        void Update()
        {
            transform.position = transform.position + (transform.right * speed * Time.deltaTime);
        }
    }
}