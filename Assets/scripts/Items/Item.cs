using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider2D))]
public class Item : MonoBehaviour
{
    public enum InteractionType
    {
        None,
        PickUp,
        Examine
    }

    public InteractionType type;


    public void Interact()
    {
        switch (type)
        {
            case InteractionType.PickUp:
                Destroy(this);
                break;

            case InteractionType.Examine:
                Debug.Log("Examine");
                break;

            default:
                Debug.Log("Null");
                break;
        }
    }

    private void Reset()
    {
        GetComponent<Collider2D>().isTrigger = true;
        gameObject.layer = 10;
    }
}